import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useCounterStore = defineStore('counter', () => {
  const count = ref(19)
  const doubleCount = computed(() => count.value * 2)

  
  const increment = ()=>{
    count.value++
  }
  const decrement = () =>{
    if(count.value <=0) return ;
    count.value--
  }
  const isOddEven = computed(()=>{
    if(count.value % 2 === 0) return "even"
    return "odd"
  })
  return { count, doubleCount, decrement ,isOddEven, increment }
})
